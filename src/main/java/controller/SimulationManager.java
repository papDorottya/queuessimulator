package controller;

import model.Client;
import model.Scheduler;
import model.Server;
import model.Strategy.SelectionPolicy;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Random;

public class SimulationManager implements Runnable {

    private static DecimalFormat df2 = new DecimalFormat(".##");

    private static int timeLimit;
    private static int maxProcessingTime;
    private static int minProcessingTime;
    public static int numberOfServers;
    public static int numberOfClients;

    private static int minArrTime;
    private static int maxArrTime;
    private SelectionPolicy seletionPolicy;

    private Scheduler scheduler;

    private static StringBuilder generator = new StringBuilder();
	private static String outFilename;

    private static ArrayList<Client> generatedClients = new ArrayList<Client>();

    public SimulationManager() {
        scheduler = new Scheduler(numberOfServers, numberOfClients);
        seletionPolicy = SelectionPolicy.SHORTEST_TIME;
    }


    private static void generateNRandomClients(int nRandomClients) throws Exception {
        int randomProcessingPers;
        int randomArrivalTime;

        for (int i = 0; i < nRandomClients; i++) {
            if (maxProcessingTime < minProcessingTime)
                throw new Exception("Incorrect MAX &&/|| MIN service time!");
            randomProcessingPers = new Random().nextInt(maxProcessingTime - minProcessingTime + 1) + minProcessingTime;

            if (maxArrTime < minArrTime)
                throw new Exception("Incorrect MAX &&/|| arrival MIN ");
            randomArrivalTime = new Random().nextInt(maxArrTime - minArrTime + 1) + minArrTime;

            generatedClients.add(new Client(i, randomArrivalTime, 0, randomProcessingPers));
        }
    }

    public void run() {
        StringBuilder sc = new StringBuilder();
        int currentTime = 0;
        int servedClients = 0;
        int waitingClients = 0;

        System.out.println("--- run start ---");
        while (currentTime <= timeLimit) {
            if (currentTime == 0) {
                sc.append("Time " + (currentTime) + "\nWaiting clients: ");
            } else {
                sc.append("\n\nTime " + (currentTime) + "\nWaiting clients: ");

            }

            Iterator<Client> iterate = SimulationManager.generatedClients.iterator();
            while (iterate.hasNext()) {
                Client client = iterate.next();
                try {
                    if (client.getArrivalTime() == currentTime) {
                        scheduler.dispatchTask(client);
                        servedClients++;
                        iterate.remove();
                        if (servedClients == numberOfClients) {
                            timeLimit = currentTime + scheduler.getMaximumWaitingTime();
                        }
                    }
                } catch (Exception exeption) {
                    if (exeption.getMessage().compareTo("All queues are full!") == 0) {
                        System.out.println(exeption.getMessage());
                    }
                }
            }

            waitingClients = 0;
            for (Client c : generatedClients) {
				sc.append(c);
			}
            for (Server s : scheduler.getServers()) {
                waitingClients += s.getSize();
                sc.append(s);
            }

            generator.append(sc.toString());
            sc.delete(0, sc.length());

            try {
                Thread.sleep(1000);
            } catch (InterruptedException exception) {
                System.out.println("Thread interrupted");
            }

            currentTime++;
        }

        double averageWaitingTime = 0;
        for (Server s : scheduler.getServers()) {
            averageWaitingTime += s.getAverageWaitingTime();
        }

        StringBuilder r = new StringBuilder();
        r.append("\n\nAverage waiting time: " + df2.format(averageWaitingTime / numberOfServers) + "\n");

        generator.append(r.toString());


		FileWriter myWriter = null;
		try {
			myWriter = new FileWriter(outFilename);
			myWriter.write(generator.toString());
			myWriter.close();
			System.out.println("--- run stop ---");
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

    public static void main(String[] args) throws IOException {
        SimStart(args);
    }

    private static void SimStart(String[] args) throws IOException {
        List<String> data = new ArrayList<>();
        String inFilename = args[0];
        outFilename = args[1];

        BufferedReader reader = new BufferedReader(new FileReader(inFilename));
        String line;
        while ((line = reader.readLine()) != null) {
            data.add(line);
        }
        reader.close();

        SimulationManager.numberOfClients = Integer.parseInt(data.get(0));
        SimulationManager.numberOfServers = Integer.parseInt(data.get(1));
        SimulationManager.timeLimit = Integer.parseInt(data.get(2));
        String[] text2 = data.get(3).split(",");
        SimulationManager.minArrTime = Integer.parseInt(text2[0]);
        SimulationManager.maxArrTime = Integer.parseInt(text2[1]);
        String[] text3 = data.get(4).split(",");
        SimulationManager.maxProcessingTime = Integer.parseInt(text3[1]);
        SimulationManager.minProcessingTime = Integer.parseInt(text3[0]);

        try {
			System.out.println("--- Start generate --- " + inFilename + "  " + outFilename);
			generateNRandomClients(numberOfClients);
			System.out.println("--- End generate ---");
		} catch (Exception e) {
            e.printStackTrace();
        }

        if (generatedClients != null) {
			System.out.println("--- Start simulation ---");
			SimulationManager s = new SimulationManager();
            Thread t = new Thread(s);
            t.start();
        }
    }

    public int getTimeLimit() {
        return timeLimit;
    }

    public void setTimeLimit(int timeLimit) {
        SimulationManager.timeLimit = timeLimit;
    }

    public int getMaxProcessingTime() {
        return maxProcessingTime;
    }

    public void setMaxProcessingTime(int maxProcessingTime) {
        SimulationManager.maxProcessingTime = maxProcessingTime;
    }

    public int getMinProcessingTime() {
        return minProcessingTime;
    }

    public void setMinProcessingTime(int minProcessingTime) {
        SimulationManager.minProcessingTime = minProcessingTime;
    }

    public int getNumberOfServers() {
        return numberOfServers;
    }

    public void setNumberOfServers(int numberOfServers) {
        SimulationManager.numberOfServers = numberOfServers;
    }

    public int getNumberOfClients() {
        return numberOfClients;
    }

    public void setNumberOfClients(int numberOfClients) {
        SimulationManager.numberOfClients = numberOfClients;
    }

    public SelectionPolicy getSeletionPolicy() {
        return seletionPolicy;
    }

    public void setSeletionPolicy(SelectionPolicy seletionPolicy) {
        this.seletionPolicy = seletionPolicy;
    }
}
