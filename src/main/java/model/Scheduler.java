package model;

import java.util.ArrayList;

public class Scheduler {

	private ArrayList<Server> servers;
	public static int maxClients;
	private Strategy strategy = new ConcreteStrategyTime();

	public Scheduler(int maxNoServers, int maxClients) {
		Scheduler.maxClients = maxClients;
		servers = new ArrayList<Server>();

		for(int i = 0; i < maxNoServers; i++) {
			Server server = new Server(i+1);
			servers.add(server);
			Thread t = new Thread(server);
			t.setName("Queue " + i);
			t.start();
		}
	}

	public void changeStrategy(Strategy.SelectionPolicy policy) {
		if(policy == Strategy.SelectionPolicy.SHORTEST_QUEUE) {
			strategy = new ConcreteStrategyQueue();
		}
		if( policy == Strategy.SelectionPolicy.SHORTEST_TIME) {
			strategy = new ConcreteStrategyTime();
		}
	}

	public void dispatchTask(Client client) throws Exception {
		strategy.addTask(servers, client);
	}

	public int getMaximumWaitingTime() {
		int maximumTime = 0;
		for(Server s: servers) {
			if(s.getWaitingTime() > maximumTime) {
				maximumTime = s.getWaitingTime();
			}
		}
		return maximumTime;
	}

	public ArrayList<Server> getServers() {
		return servers;
	}
}