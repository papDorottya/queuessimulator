package model;

import java.util.ArrayList;
import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.atomic.AtomicInteger;

public class Server implements Runnable {
	private int id;
	private BlockingQueue<Client> clients;
	private AtomicInteger waitingPeriod;
	public static double averageWaitingTime;

	public Server(int id) {
		this.id = id;
		waitingPeriod = new AtomicInteger(0);
		clients = new ArrayBlockingQueue<Client>(Scheduler.maxClients);
	}

	public void addClient(Client newClient) {
		clients.add(newClient);
		newClient.setFinishTime(newClient.getArrivalTime() + newClient.getProcessingPeriod() + this.waitingPeriod.intValue());
		this.waitingPeriod.set(this.waitingPeriod.intValue() + newClient.getProcessingPeriod());
	}

	public void run() {
		int noOfClientsProcessed = 0;
		double totalWaitingTime = 0;
		Client client;
		while (true) {
			try {
				client = clients.peek();
				if(client != null) {
					noOfClientsProcessed++;
					totalWaitingTime += client.getFinishTime() - client.getArrivalTime();
					Server.averageWaitingTime = totalWaitingTime / noOfClientsProcessed;
					Thread.sleep(1000 * client.getProcessingPeriod());
					this.waitingPeriod.set(this.waitingPeriod.intValue() - client.getProcessingPeriod());
					clients.poll();
				}
			} catch (InterruptedException exception) {
				exception.printStackTrace();
			}
		}
	}

	public int getSize() {
		return this.clients.size();
	}

	public int getWaitingTime() {
		return this.waitingPeriod.intValue();
	}

	public double getAverageWaitingTime() {
		return Server.averageWaitingTime;
	}

	public ArrayList<Client> getClients() {
		ArrayList<Client> client1 = new ArrayList<Client>(clients);
		return client1;
	}

	@Override
	public String toString() {
		StringBuilder s = new StringBuilder();
		s.append("\nQueue " + this.id + ": ");
		if(clients.size() == 0) {
			s.append("closed");
		}
		else {
			for (Client c : clients) {
				s.append(c.toString());
			}
		}
		return s.toString();
	}

}